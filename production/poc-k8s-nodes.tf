module "nodes" {

  source = "./modules/pve-vm"

  count = 6

  template = "template-ubuntu-focal-amd64-cloudinit"
  hostname = "node${format("%02d", count.index + 1)}.pok8s.prod.priv.cwxlab.fr"
  node = "node301"
  pool = "POC-K8S"

  memory  = "8192"
  balloon = "4096"

  ipconfig0 = "ip=10.10.101.2${format("%02d", count.index + 1)}/24"
  ipconfig1 = "ip=10.10.111.2${format("%02d", count.index + 1)}/24,gw=10.10.111.240"

  bridges = ["vmbr101", "vmbr111"]

  disks   = [
    {
      size: "32G",
      storage: "local"
    },
  ]

  user_name     = "pok8s"
  user_ssh_keys = var.ssh_public_keys
}
