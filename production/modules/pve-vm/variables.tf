variable "hostname" {
  type        = string
  description = "PVE VM hostname"
}

variable "node" {
  type        = string
  description = "PVE VM target node"
}

variable "pool" {
  type        = string
  description = "PVE VM Ressource Pool"
}

variable "ipconfig0" {
  type        = string
  description = "PVE VM first network card IP configuration"
}

variable "ipconfig1" {
  type        = string
  default     = null
  description = "[Optional] PVE VM second network card IP configuration"
}

variable "ipconfig2" {
  type        = string
  default     = null
  description = "[Optional] PVE VM third network card IP configuration"
}

variable "ipconfig3" {
  type        = string
  default     = null
  description = "[Optional] PVE VM forth network card IP configuration"
}

variable "bridges" {
  type        = list
  default     = []
  description = "PVE VM network bridges. The order is : [ipconfig0, ipconfig1, ipconfig2]"
}

variable "disks" {
  type        = list
  default     = ["32G"]
  description = "PVE VM disks size"
}

variable "user_name" {
  type        = string
  default     = "ubuntu"
  description = "PVE VM user name"
}

variable "user_ssh_keys" {
  type        = string
  description = "PVE VM SSH public Key"
}

variable "cores" {
  type        = number
  default     = 2
  description = "PVE VM number of CPU cores"
}

variable "sockets" {
  type        = number
  default     = 2
  description = "PVE VM number of sockets per CP core"
}

variable "memory" {
  type        = number
  default     = 4096
  description = "PVE VM amount of memory in MB"
}
variable "balloon" {
  type        = number
  default     = 8192
  description = "PVE VM amount of balloon memory in MB"
}

variable "template" {
  type        = string
  description = "PVE VM template to use"
}
