resource "proxmox_vm_qemu" "pve-vm" {

  target_node = var.node
  pool        = var.pool
  name        = var.hostname
  
  os_type  = "cloud-init"
  clone    = var.template

  cpu      = "host"
  cores    = var.cores
  sockets  = var.sockets
  
  memory   = var.memory
  balloon  = var.balloon
  
  scsihw   = "virtio-scsi-pci"
  bootdisk = "scsi0"
  define_connection_info = false

  dynamic "disk" {
    for_each = var.disks

    content {
      size    = disk.value["size"]
      type    = "scsi"
      storage =  disk.value["storage"]
    }
  }

  dynamic "network" {
    for_each = var.bridges

    content {
      model  = "virtio"
      bridge = network.value
    }
  }

  lifecycle {
    ignore_changes = [
      network,
    ]
  }

  # Cloud Init Settings
  ipconfig0 = var.ipconfig0 != "" ? var.ipconfig0 : false
  ipconfig1 = var.ipconfig1 != "" ? var.ipconfig1 : false
  ipconfig2 = var.ipconfig2 != "" ? var.ipconfig2 : false
  ipconfig3 = var.ipconfig3 != "" ? var.ipconfig3 : false

  ciuser = var.user_name

  sshkeys = <<EOF
  ${var.user_ssh_keys}
  EOF

}
